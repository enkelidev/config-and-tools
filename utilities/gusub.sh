#!/bin/bash

####
# Run git pull on all direct subdirectories of the current folder.
###

find . -mindepth 1 -maxdepth 1 -type d -print -exec git -C {} pull \;
