#!/bin/zsh

SESSION=`basename $PWD`

tmux -2 new-session -d -s $SESSION

tmux rename-window -t $SESSION:1 Shell
tmux split-window -h
tmux new-window -t $SESSION -a -n Vim 'source ~/.zshrc; vim --servername VIM'
tmux new-window -t $SESSION -a -n Ranger 'source ~/.zshrc; ranger'
tmux new-window -t $SESSION -a -n Docker
tmux split-window -h

tmux select-window -t $SESSION:1
tmux -2 attach -t $SESSION
