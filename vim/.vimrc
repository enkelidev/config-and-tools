let g:netrw_banner = 0

let mapleader = " "

noremap j gj
noremap k gk

"Fuzzy files
set path+=**
set wildmenu

"-------------------
"Custom key bindings
"-------------------

"Write file (save)
nmap <c-s> :w<CR>
imap <c-s> <Esc><c-s>

"Buffers
nmap <c-x> :bdelete<CR>
map <BS> :bprev!<Return>
map <C-l> :bnext!<Return>

"FZF Search
map <Leader>1 :GFiles<Return>
map <Leader>2 :Buffers<Return>
map <Leader>3 :Lines<Return>

"File Explorer
map <Leader>` :Ex<Return>

"Power movement
map <Leader>h 20h
map <Leader>j 7j
map <Leader>k 7k
map <Leader>l 20l

set expandtab
set tabstop=4
set shiftwidth=4

set t_Co=256
syntax on
set number
set encoding=utf-8

set noruler
set noshowcmd

set ttimeoutlen=10

set cursorline
hi CursorLine ctermbg=8 term=bold cterm=bold "8 = dark gray, 15 = white
hi Cursor ctermbg=15 

set cursorcolumn
